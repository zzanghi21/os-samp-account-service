package com.sk.dep.sample.account.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.nio.ByteBuffer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration  {
    @Bean
    public Docket swaggerSpringfoxApiDocket(){
        Docket docket = createDocket()
                .apiInfo(apiInfo())
                .forCodeGeneration(true)
                .directModelSubstitute(ByteBuffer.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .select()
                .paths(regex("/api/.*"))
                .build();
            return docket;
    }
    
    @Bean
    public Docket swaggerSpringfoxManagementDocket(@Value("${spring.application.name}") String appName,
            @Value("${management.context-path:#{null}}") String managementContextPath) {

    	Docket docket =  createDocket()
    		.apiInfo(apiInfo())
            .groupName("management")
            .forCodeGeneration(true)
            .directModelSubstitute(java.nio.ByteBuffer.class, String.class)
            .genericModelSubstitutes(ResponseEntity.class);
            ;
         if(managementContextPath!=null) {   
        	 return docket.select()
        	 .paths(regex(managementContextPath + ".*"))
        	 .build();
         } 
         return docket.select()
                	 .build();
         
    }    
 
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Samp Account Service")
                .description("REST API")
                .version("v1").build();
    }
    private Docket createDocket() {
        return new Docket(DocumentationType.SWAGGER_2);
    }
}
